# Anagram Rest Service

#The task
You need to put together a simple web application that can produce anagrams of words from a wordlit.

·  You can find a copy of our wordlist at http://static.abscond.org/wordlist.txt

·  Non-alphanumeric characters should be considered as part of the anagram (e.g. "he's" is not an anagram of "she")

·  The application should be able to respond to a request made every second

·  Assume that the application will be hosted on heroku

#Documentation
The application should be able to receive an HTTP GET request with the requested word as the path. It should return the results as JSON. See the example below:
```
GET /crepitus
{"crepitus":["cuprites","pictures","piecrust"]}
  
  
GET /crepitus,paste,kinship,enlist,boaster,fresher,sinks,knits,sort
{"crepitus":["cuprites","pictures","piecrust"],"paste":["pates","peats","septa","spate","tapes","tepas"],"kinship":["pinkish"],"enlist":["elints","inlets","listen","silent","tinsel"],"boaster":["boaters","borates","rebatos","sorbate"],"fresher":["refresh"],"sinks":["skins"],"knits":["skint","stink","tinks"],"sort":["orts","rots","stor","tors"]}
  
  
GET /sdfwehrtgegfg
{"sdfwehrtgegfg":[]}
```


#TEST
The application has been deployed to an Heroku free tier at this address https://glacial-headland-76929.herokuapp.com/ 

Performarce Testing using Apache Benchmark 

```
ab -n 10000 -c 100 -v 2 https://glacial-headland-76929.herokuapp.com/crepitus,paste,kinship,enlist,boaster,fresher,sinks,knits,sort


Concurrency Level:      100
Time taken for tests:   152.797 seconds
Complete requests:      10000
Failed requests:        0
Total transferred:      4920000 bytes
HTML transferred:       3260000 bytes
Requests per second:    65.45 [#/sec] (mean)
Time per request:       1527.969 [ms] (mean)
Time per request:       15.280 [ms] (mean, across all concurrent requests)
Transfer rate:          31.44 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:      405  906 2365.8    726   36527
Processing:   130  271  98.6    279     929
Waiting:      129  261  93.9    268     912
Total:        540 1177 2360.6   1035   36663

Percentage of the requests served within a certain time (ms)
  50%   1035
  66%   1061
  75%   1077
  80%   1089
  90%   1143
  95%   1292
  98%   2025
  99%   3288
 100%  36663 (longest request)
 ```