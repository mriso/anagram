package com.risomichele.util;

import com.risomichele.Main;
import org.apache.maven.shared.utils.io.DirectoryScanner;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.nio.charset.StandardCharsets.UTF_8;

/**
 * Created by Michele Riso on 27/04/2018.
 */
public class WordListLoader {

    private static final String WORDLISTFILE = "wordlist.txt";

    public static final HashMap<String, HashSet<String>> loadWordList() throws IOException {

        HashMap<String, HashSet<String>> wordMap = new HashMap<String, HashSet<String>>();

        Resource resource = new PathResource(WordListLoader.find(WORDLISTFILE));
        File file = resource.getFile();
        Stream<String> stream = Files.lines(file.toPath());

            stream.map(String::toLowerCase)
                    .forEach(word -> {
                        String s = WordListLoader.prepareString(word);
                        if(wordMap.get(s) == null){
                            HashSet<String> values = new HashSet<String>();
                            values.add(word);
                            wordMap.put(s, values);

                        }else{
                           Set<String> values = wordMap.get(s);
                            values.add(word);
                        }

                    });

        return wordMap;
    }


    public static final String prepareString(String word){
        char[] sorted = word.toCharArray();
        Arrays.sort(sorted);
        return  new String(sorted);
    }


    private static final String find(String filename) {
        String path = ".";
        DirectoryScanner scanner = new DirectoryScanner();
        scanner.setIncludes("**/" + filename);
        scanner.setBasedir(path);
        scanner.setCaseSensitive(false);
        scanner.scan();
        String[] files = scanner.getIncludedFiles();
        return files[0];
    }

}
