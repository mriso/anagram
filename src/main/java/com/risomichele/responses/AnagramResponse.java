package com.risomichele.responses;

import com.risomichele.beans.Anagram;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Michele Riso on 27/04/2018.
 */
public class AnagramResponse {

    private List<Anagram> anagrams;

    public AnagramResponse(List<Anagram> anagrams) {
        this.anagrams = anagrams;
    }

    @Override
    public String toString() {
        return "{" +
                anagrams.stream()
                        .map(Anagram::toString)
                        .collect(Collectors.joining(", ")) +
                '}';
    }
}
