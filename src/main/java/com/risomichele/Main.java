/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * Created by Michele Riso on 27/04/2018.
 */
package com.risomichele;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import static com.risomichele.util.WordListLoader.loadWordList;

@SpringBootApplication
public class Main {

  public static HashMap<String, HashSet<String>> WORDMAP;

  public static void main(String[] args) {

    try {
      Main.WORDMAP = loadWordList();
      SpringApplication.run(Main.class, args);
    } catch (IOException e) {
      e.printStackTrace();
    }

  }
}