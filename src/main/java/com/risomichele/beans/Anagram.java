package com.risomichele.beans;

import java.util.ArrayList;
import java.util.HashSet;

/**
 * Created by Michele Riso on 27/04/2018.
 */
public class Anagram {

        private String request;
        private HashSet<String> anagrams;

        public Anagram(String request, HashSet<String> anagrams) {
                this.request = request;
                if(anagrams == null){
                        this.anagrams = new HashSet<String>();
                }else{
                        this.anagrams = anagrams;
                        this.anagrams.remove(request);
                }
        }

        @Override
        public String toString() {
                return "" +
                        "\"" + request + '\"' +
                        ":" + anagrams +
                        "";
        }
}
