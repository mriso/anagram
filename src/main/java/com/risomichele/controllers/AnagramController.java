package com.risomichele.controllers;


import com.risomichele.Main;
import com.risomichele.beans.Anagram;
import com.risomichele.responses.AnagramResponse;
import com.risomichele.util.WordListLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by Michele Riso on 27/04/2018.
 */

import org.springframework.web.bind.annotation.RequestMapping;
        import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.*;


@RestController
public class AnagramController {

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public ResponseEntity getAnagram() {

        return new ResponseEntity<String>("{error: 'use with /word-to-search'}",HttpStatus.BAD_REQUEST) ;
    }


    @RequestMapping(value = "/{words}", method = RequestMethod.GET)
    public ResponseEntity getAnagram(@PathVariable("words")  String words) {

        List<String> request = Arrays.asList(words.split(","));
        List<Anagram> aL = new LinkedList<Anagram>();

        request.forEach( word -> {
            aL.add(new Anagram(word, Main.WORDMAP.get(WordListLoader.prepareString(word.toLowerCase()))));
        } );

        return new ResponseEntity<String>((new AnagramResponse(aL)).toString(), HttpStatus.OK) ;
    }

}